const { fighter } = require("../models/fighter");

const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation
    const err = validateData(req.body, fighter);
    if (err.error) {
        res.status(400).json(err);
    } else {
        next();
    }
};

const validateData = (fighterData, fighterModel) => {
    if (
        !fighterData.hasOwnProperty("name") ||
        !fighterData.hasOwnProperty("power") ||
        !fighterData.hasOwnProperty("defense") ||
        Object.keys(fighterData).length === 0
    ) {
        return fighterError("model");
    }
    for (const key of Object.keys(fighterData)) {
        if (fighterModel[key] === undefined) {
            return fighterError(key);
        } else {
            switch (key) {
                case "id":
                    return fighterError(key);
                case "name":
                    if (fighterData[key].length === 0) {
                        return fighterError(key);
                    }
                    break;
                case "health":
                    if (
                        typeof fighterData[key] !== "number" ||
                        fighterData[key] < 80 ||
                        fighterData[key] > 120
                    ) {
                        return fighterError(key);
                    }
                    break;
                case "power":
                    if (
                        typeof fighterData[key] !== "number" ||
                        fighterData[key] < 1 ||
                        fighterData[key] > 100
                    ) {
                        return fighterError(key);
                    }
                    break;
                case "defense":
                    if (
                        typeof fighterData[key] !== "number" ||
                        fighterData[key] < 1 ||
                        fighterData[key] > 10
                    ) {
                        return fighterError(key);
                    }
                    break;
            }
        }
    }
    return { error: false };
};

const fighterError = (field) => ({
    error: true,
    message: `Fighter ${field} is invalid`,
});

const updateFighterValid = createFighterValid;

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;
