const { user } = require("../models/user");

const createUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during creation

    const err = validateData(req.body, user);
    if (err.error) {
        res.status(400).json(err);
    } else {
        next();
    }
};

const validateData = (userData, userModel) => {
    if (
        !userData.hasOwnProperty("firstName") ||
        !userData.hasOwnProperty("lastName") ||
        !userData.hasOwnProperty("email") ||
        !userData.hasOwnProperty("phoneNumber") ||
        !userData.hasOwnProperty("password") ||
        Object.keys(userData).length === 0
    ) {
        return userError("model");
    }
    for (const key of Object.keys(userData)) {
        if (userModel[key] === undefined) {
            return userError(key);
        } else {
            switch (key) {
                case "id":
                    return userError(key);
                case "firstName":
                    if (userData[key].length === 0) {
                        return userError(key);
                    }
                    break;
                case "lastName":
                    if (userData[key].length === 0) {
                        return userError(key);
                    }
                    break;
                case "password":
                    if (userData[key].length < 3) {
                        return userError(key);
                    }
                    break;
                case "email":
                    if (
                        userData[key].length === 0 ||
                        !/^\w+([\.-]?\w+)*@gmail.com$/.test(userData[key])
                    ) {
                        return userError(key);
                    }
                    break;
                case "phoneNumber":
                    if (
                        userData[key].length === 0 ||
                        !/^\+380\d{9}/.test(userData[key])
                    ) {
                        return userError(key);
                    }
                    break;
            }
        }
    }
    return { error: false };
};

const userError = (field) => ({
    error: true,
    message: `User ${field} is invalid`,
});

const updateUserValid = createUserValid;

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
