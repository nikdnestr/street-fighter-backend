const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

// TODO: Implement route controllers for fighter

router.get("/", (req, res) => {
    const fighters = FighterService.getFighters();
    if (fighters) {
        res.status(200).json(fighters);
    } else {
        res.json(400).json({
            error: true,
            message: "Sorry, but there is no fighters found",
        });
    }
});

router.get("/:id", (req, res) => {
    const fighter = FighterService.getFighter({ id: req.params.id });
    if (fighter) {
        res.status(200).json(fighter);
    } else {
        res.json(404).json({
            error: true,
            message: "Sorry, but the fighter is not found",
        });
    }
});

router.post("/", createFighterValid, (req, res) => {
    const fighter = FighterService.createFighter(req.body);
    if (fighter) {
        res.status(200).json(fighter);
    } else {
        res.json(400).json({
            error: true,
            message: "Sorry, but you cant make a new fighter right now",
        });
    }
});

router.put("/:id", updateFighterValid, (req, res) => {
    const fighter = FighterService.updateFighter(req.params.id, req.body);
    if (fighter) {
        res.status(200).json(fighter);
    } else {
        res.json(404).json({
            error: true,
            message: "Sorry, but the fighter is not found",
        });
    }
});

router.delete("/:id", (req, res) => {
    const fighter = FighterService.removeFighter(req.params.id);
    if (fighter) {
        res.status(200).json(fighter);
    } else {
        res.json(404).json({
            error: true,
            message: "Sorry, but the fighter is not found",
        });
    }
});

module.exports = router;