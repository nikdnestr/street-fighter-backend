const { Router } = require("express");
const UserService = require("../services/userService");
const {
    createUserValid,
    updateUserValid,
} = require("../middlewares/user.validation.middleware");
const { responseMiddleware } = require("../middlewares/response.middleware");

const router = Router();
const clearUser = ({ id, password, ...other }) => other;


// TODO: Implement route controllers for user
router.get("/", (req, res) => {
    const users = UserService.getUsers();
    if (users) {
        res.json(users);
    } else {
        res.json(400).json({
            error: true,
            message: "Sorry, but there is no users found",
        });
    }
});

router.get("/:id", (req, res) => {
    const user = UserService.getUser({ id: req.params.id });
    if (user) {
        res.json(user);
    } else {
        res.json(404).json({
            error: true,
            message: "Sorry, but the user is not found",
        });
    }
});

router.post("/", createUserValid, (req, res) => {
    const user = UserService.createUser(req.body);
    if (user) {
        res.json(user);
    } else {
        res.json(400).json({
            error: true,
            message: "Sorry, but you cant make a new user right now",
        });
    }
});

router.put("/:id", updateUserValid, (req, res) => {
    const user = UserService.updateUser(req.params.id, req.body);
    if (user) {
        res.json(user);
    } else {
        res.json(404).json({
            error: true,
            message: "Sorry, but the user is not found",
        });
    }
});

router.delete("/:id", (req, res) => {
    const user = UserService.removeUser(req.params.id);
    if (user) {
        res.json(user);
    } else {
        res.json(404).json({
            error: true,
            message: "Sorry, but the user is not found",
        });
    }
});

module.exports = router;
