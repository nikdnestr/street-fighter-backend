const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    getFighters() {
        const fighters = FighterRepository.getAll();
        if (!fighters) {
            return null;
        }
        return fighters;
    }

    getFighter(id) {
        const fighter = FighterRepository.getOne(id);
        if (!fighter) {
            return null;
        }
        return fighter;
    }

    createFighter(fighter) {
        return FighterRepository.create(fighter);
    }

    updateFighter(id, fighterData) {
        const updatedFighter = FighterRepository.update(id, fighterData);
        if (!updatedFighter) {
            return null;
        }
        return updatedFighter;
    }

    removeFighter(id) {
        const removedFighter = FighterRepository.delete(id);
        if (!removedFighter) {
            return null;
        }
        return removedFighter;
    }
}

module.exports = new FighterService();