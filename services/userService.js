const { UserRepository } = require("../repositories/userRepository");

class UserService {
    // TODO: Implement methods to work with user
    getUsers() {
        const users = UserRepository.getAll();
        if (!users) {
            return null;
        }
        return users;
    }

    getUser(id) {
        const user = UserRepository.getOne(id);
        if (!user) {
            return null;
        }
        return user;
    }

    createUser(user) {
        return UserRepository.create(user);
    }

    updateUser(id, userData) {
        const updatedUser = UserRepository.update(id, userData);
        if (!updatedUser) {
            return null;
        }
        return updatedUser;
    }

    removeUser(id) {
        const removedUser = UserRepository.delete(id);
        if (!removedUser) {
            return null;
        }
        return removedUser;
    }
}

module.exports = new UserService();